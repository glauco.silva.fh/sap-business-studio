## Application Details
|               |
| ------------- |
|**Generation Date and Time**<br>Fri Feb 26 2021 00:54:05 GMT+0000 (Coordinated Universal Time)|
|**App Generator**<br>@sap/generator-fiori|
|**App Generator Version**<br>1.1.3|
|**Generation Platform**<br>SAP Business Application Studio|
|**Floorplan Used**<br>List Report Object Page V2|
|**Service Type**<br>OData Url|
|**Service URL**<br>https://services.odata.org/V2/Northwind/Northwind.svc/
|**Module Name**<br>myfirstproject|
|**Application Title**<br>First Project|
|**Namespace**<br>com.eggo.products|
|**UI5 Theme**<br>sap_fiori_3_dark|
|**UI5 Version**<br>1.75.5|
|**Enable Telemetry**<br>False|
|**Main Entity**<br>Products|
|**Navigation Entity**<br>Supplier|

## myfirstproject

A Fiori application.

### Starting the generated app

-   This app has been generated using the SAP Fiori tools - App Generator, as part of the SAP Fiori tools suite.  In order to launch the generated app, simply run the following from the generated app root folder:

```
    npm start
```


#### Pre-requisites:

1. Active NodeJS LTS (Long Term Support) version and associated supported NPM version.  (See https://nodejs.org)


